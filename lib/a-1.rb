# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  smallest = nums[0]
  largest = nums[-1]
  array_numbers = smallest.upto(largest).to_a
  array_numbers.select! do |num|
    if nums.include?(num) == false
      num
    end
  end
  array_numbers
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  result = 0
  binary.reverse.split('').each_with_index do |digit, index|
    result += (digit.to_i * 2**index)
  end
  result
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    hash = {}
    self.each do |k, v|
      if prc.call(k,v)
        hash[k] = v
      end
    end
    hash
  end
  end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    merged = {}
    allkeys = self.keys + hash.keys
    #if the block is given then we yield the value if that key is in both values,
    #otherwise just set the key equal to its corresponding value
    if block_given?
      allkeys.uniq.each do |k|
        if self.has_key?(k) && hash.has_key?(k)
          merged[k] = yield(k,self[k],hash[k])
        elsif self.has_key?(k) == true && hash.has_key?(k) == false
          merged[k] = self[k]
        elsif hash.has_key?(k) == true && self.has_key?(k) == false
          merged[k] = hash[k]
        end
      end
    else
      #if no bloc is given we simply set each key in new hash equal to corresponding value
      allkeys.uniq.each do |k|
        if self.has_key?(k) && hash.has_key?(k)
          merged[k] = hash[k]
        elsif self.has_key?(k) == true && hash.has_key?(k) == false
          merged[k] = self[k]
        elsif hash.has_key?(k) == true && self.has_key?(k) == false
          merged[k] = hash[k]
        end
      end
    end
    return merged
  end
end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  lucas_arr = [2,1]
  if n > 1
  while lucas_arr.length - 1 < n
    lucas_arr.push(lucas_arr[-1] + lucas_arr[-2])
  end
  return lucas_arr[-1]
  elsif n <=1 && n >= 0
    return lucas_arr[n]
  else
    n = (-n)+2
    i = 1
    while lucas_arr.length < n
      lucas_arr.unshift(lucas_arr[lucas_arr.length - i]-lucas_arr[lucas_arr.length - i-1])
      i += 1
    end
    return lucas_arr[0]
  end
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  i = 0
  j = 3
  array = string.split('')
  palindrome = []
  while i < array.length - 3
    while j < array.length
      if array[i..j] == array[i..j].reverse
        palindrome << array[i..j]
      end
      j += 1
    end
    i += 1
    j = i + 3
  end
  return false if palindrome.empty?
  palindrome.sort[0].length
end
